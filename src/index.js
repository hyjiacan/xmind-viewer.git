import parser from "./parser";
import viewer from "./viewer";
import './viewer.css'

export default {
  parser,
  viewer
}
